/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.35-MariaDB : Database - parqueadero
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`parqueadero` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `parqueadero`;

/*Table structure for table `funcionarios` */

DROP TABLE IF EXISTS `funcionarios`;

CREATE TABLE `funcionarios` (
  `idFuncionario` int(11) NOT NULL AUTO_INCREMENT,
  `funIdentificacion` varchar(15) NOT NULL,
  `funNombres` varchar(45) DEFAULT NULL,
  `funApellidos` varchar(45) DEFAULT NULL,
  `funCorreo` varchar(45) DEFAULT NULL,
  `funCargo` varbinary(45) DEFAULT NULL,
  PRIMARY KEY (`idFuncionario`),
  UNIQUE KEY `funIdentificacion_UNIQUE` (`funIdentificacion`),
  UNIQUE KEY `funCorreoUNIQUE` (`funCorreo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `funcionarios` */

insert  into `funcionarios`(`idFuncionario`,`funIdentificacion`,`funNombres`,`funApellidos`,`funCorreo`,`funCargo`) values (3,'1079','Jesus Ariel','Gonzalez','fdkljsafjhdsjf@','nuevo'),(4,'234','jesus','234','jesus','234'),(5,'321321','3dsad','sdsa','dasd','sadas');

/*Table structure for table `parqueocarros` */

DROP TABLE IF EXISTS `parqueocarros`;

CREATE TABLE `parqueocarros` (
  `idParqueoCarros` int(11) NOT NULL AUTO_INCREMENT,
  `parPlaca` varchar(6) DEFAULT NULL,
  `parFechaHoraIngreso` timestamp NULL DEFAULT NULL,
  `parFechaHoraSalida` timestamp NULL DEFAULT NULL,
  `parFuncionario` int(11) DEFAULT NULL,
  PRIMARY KEY (`idParqueoCarros`),
  UNIQUE KEY `parPlacaUNIQUE` (`parPlaca`),
  KEY `fk_parquo_funcionario` (`parFuncionario`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `parqueocarros` */

insert  into `parqueocarros`(`idParqueoCarros`,`parPlaca`,`parFechaHoraIngreso`,`parFechaHoraSalida`,`parFuncionario`) values (2,'ewqewq','2019-02-15 00:00:00','2019-02-15 00:00:00',123214),(5,'sad','2019-02-07 00:00:00','2019-02-06 00:00:00',1079),(6,'wrwes','2019-02-15 00:00:00','2019-02-14 00:00:00',11),(7,'3rewr','2019-02-06 00:00:00','2019-02-14 00:00:00',12),(8,'wewqe','2019-02-07 00:00:00','2019-02-08 00:00:00',432);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
