<?php
require('configuracion.php');

class Conexion{
    
    public $conn = null;
    public $recordSet = null;
    public $sentenciaSql = null;
    private $message = null;
    
    function __construct() {        
        $this->conn=mysqli_connect(SERVERNAME,USER,PASSWORD,DATABASE);         
        
        if (!$this->conn) {
            // $this->message = mysqli_error_list();			
            // $error = $this->obtenerError();			
            throw new Exception('No fué posible conectar a la base de datos: ', E_USER_ERROR);
        }
    }
    
    public function preparar($sentenciaSql){
        $this->sentenciaSql = $sentenciaSql;
    }

    public function ejecutar() {        
        $this->recordSet = mysqli_query($this->conn, $this->sentenciaSql);       
        if(!$this->recordSet){            
            // $error = $this->obtenerError();
            // throw new Exception('No fué posible guardar la información. '.$error['message'], E_USER_ERROR);
            throw new Exception('No fué posible guardar la información.');
        }
        return $this->recordSet;
    }
    
    public function obtenerObjeto() {
        return mysqli_fetch_object($this->recordSet);
    }
    
    public function obtenerArray() {
        return mysqli_fetch_array($this->recordSet);
    }

    public function obtenerRegistros(){
        return mysqli_fetch_all($this->recordSet);
    }
    
    public function obtenerRow() {
        return mysqli_fetch_row($this->recordSet);
    }
    public function obtenerNumeroRegistros(){
        return mysqli_num_rows($this->recordSet);
    }  
    function __destruct() {
        if ($this->conn)
            mysqli_close($this->conn);
    }
    
    // private function obtenerError(){
    //     $resultado = array();
    //     if( ($errors = mysqli_error() ) != null){
    //         foreach( $errors as $error){
    //             $resultado['SQLSTATE'] = $error[ 'SQLSTATE'];
    //             $resultado['code'] = $error[ 'code'];
    //             $resultado['message'] = $error[ 'message'];
    //         }
    //     }
    //     return $resultado;
    // }
    
}
?>
