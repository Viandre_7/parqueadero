        <?php
        class Funcionario{

        private $idFuncionario;
        private $funIdentificacion;
        private $funNombres;
        private $funApellidos;
        private $funCorreo;
        private $funCargo;
        public  $conn=null;

        //idFuncionario
        public function getIdFuncionario(){return $this->idFuncionario;}
        public function setIdFuncionario($idFuncionario){$this->idFuncionario = $idFuncionario;}

        //funIdentificacion
        public function getFunIdentificacion(){return $this->funIdentificacion;}
        public function setFunIdentificacion($funIdentificacion){$this->funIdentificacion = $funIdentificacion;}

        //funNombres
        public function getFunNombres(){return $this->funNombres;}
        public function setFunNombres($funNombres){$this->funNombres = $funNombres;}

        //funApellidos
        public function getFunApellidos(){ return $this->funApellidos;}
        public function setFunApellidos($funApellidos) { $this->funApellidos =$funApellidos;}

        //funCorreo
        public function getFunCorreo(){ return $this->funCorreo;}
        public function setFunCorreo($funCorreo) { $this->funCorreo =$funCorreo;}

        //funCargo
        public function getFunCargo(){ return $this->funCargo;}
        public function setFunCargo($funCargo) { $this->funCargo =$funCargo;}


        public function __construct() {
        $this->conn = new Conexion();
        }

        public function adicionar(){
        $sentenciaSql = "INSERT INTO funcionarios 
        (funIdentificacion, funNombres, funApellidos, funCorreo, funCargo) 
        VALUES 
        ('$this->funIdentificacion', '$this->funNombres','$this->funApellidos','$this->funCorreo', '$this->funCargo')
        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
        }

        public function modificar(){
        $sentenciaSql = "UPDATE funcionarios 
        SET 
        funIdentificacion = '$this->funIdentificacion', 
        funNombres = '$this->funNombres',
        funApellidos = '$this->funApellidos', 
        funCorreo = '$this->funCorreo',
        funCargo = '$this->funCargo' 
        WHERE idFuncionario = $this->idFuncionario  
        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        }

        public function eliminar(){
        $sentenciaSql = "DELETE FROM 
        funcionarios 
        WHERE 
        idFuncionario = $this->idFuncionario";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        }

        public function consultar(){

        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
        idFuncionario
        ,funIdentificacion
        ,funNombres
        ,funApellidos
        ,funCorreo
        ,funCargo
        FROM
        funcionarios $condicion					
        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
        }    

        private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idFuncionario !=''){
        $condicion=$condicion.$whereAnd." funcionarios.idFuncionario  = $this->idFuncionario";
        $whereAnd = ' AND ';

        }else{
        if($this->funIdentificacion !=''){
        $condicion=$condicion.$whereAnd." funcionarios.funIdentificacion  = '$this->funIdentificacion'";
        $whereAnd = ' AND ';            
        }
        if($this->funNombres !=''){
        $condicion=$condicion.$whereAnd." funcionarios.funNombres  like '%$this->funNombres%'";
        $whereAnd = ' AND ';            
        }
        if($this->funApellidos !=''){
        $condicion=$condicion.$whereAnd." funcionarios.funApellidos  like '%$this->funApellidos%'";
        $whereAnd = ' AND ';            
        }
        if($this->funCorreo !=''){
        $condicion=$condicion.$whereAnd." funcionarios.funCorreo  like '%$this->funCorreo%'";
        $whereAnd = ' AND ';            
        }
        if($this->funCargo !=''){
        $condicion=$condicion.$whereAnd." funcionarios.funCargo  like '%$this->funCargo%'";
        $whereAnd = ' AND ';            
        }

        }       
        return $condicion;

        }

        public function __destruct() {
        unset($this->idFuncionario);
        unset($this->funIdentificacion);
        unset($this->funNombres);
        unset($this->funApellidos);
        unset($this->funCorreo);
        unset($this->funCargos);
        unset($this->conn);
        }
        }
        ?>
