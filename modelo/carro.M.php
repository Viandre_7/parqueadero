<?php
class Carro{
    private $idCarro;
    private $marca;
    public  $conn=null;

    //idCarro
    public function getIdCarro(){return $this->idCarro;}
    public function setIdCarro($idCarro){$this->idCarro = $idCarro;}

    //marca
    public function getMarca(){return $this->marca;}
    public function setMarca($marca){$this->marca = $marca;}


    public function __construct() {
        $this->conn = new Conexion();
    }

    public function adicionar(){
        $sentenciaSql = "INSERT INTO carro(
                        id_carro
                        ,marca
                    )VALUES(
                        $this->idCarro
                        ,'$this->marca'
                    )";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }       

    public function __destruct() {
        unset($this->idCarro);
        unset($this->marca);
        unset($this->conn);
    }
}
?>
