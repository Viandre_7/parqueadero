<?php
class ParqueoCarros{
    
    private $idFuncionario;
    private $funIdentificacion;
    private $funNombres;
    private $funApellidos;
    private $funCorreo;
    private $funCargo;
    public  $conn=null;
    
    //idParqueoCarros
    public function getIdParqueoCarros(){return $this->idParqueoCarros;}
    public function setIdParqueoCarros($idParqueoCarros){$this->idParqueoCarros = $idParqueoCarros;}
    
    //parPlaca
    public function getParPlaca(){return $this->parPlaca;}
    public function setParPlaca($parPlaca){$this->parPlaca = $parPlaca;}
    
    //parFechaHoraIngreso
    public function getParFechaHoraIngreso(){return $this->parFechaHoraIngreso;}
    public function setParFechaHoraIngreso($parFechaHoraIngreso){$this->parFechaHoraIngreso = $parFechaHoraIngreso;}
    
    //parFechaHoraSalida
    public function getParFechaHoraSalida(){ return $this->parFechaHoraSalida;}
    public function setParFechaHoraSalida($parFechaHoraSalida) { $this->parFechaHoraSalida =$parFechaHoraSalida;}
    
    //parFuncionario
    public function getParFuncionario(){ return $this->parFuncionario;}
    public function setParFuncionario($parFuncionario) { $this->parFuncionario =$parFuncionario;}
    
    public function __construct() {
        $this->conn = new Conexion();
    }
    
    public function adicionar(){
        $sentenciaSql = "INSERT INTO parqueocarros 
                            (parPlaca, parFechaHoraIngreso, parFechaHoraSalida, parFuncionario) 
                            VALUES 
                            ('$this->parPlaca','$this->parFechaHoraIngreso', '$this->parFechaHoraSalida','$this->parFuncionario')
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
       $sentenciaSql = "UPDATE parqueocarros 
                            SET 
                                parPlaca = '$this->parPlaca', 
                                parFechaHoraIngreso = '$this->parFechaHoraIngreso',
                                parFechaHoraSalida = '$this->parFechaHoraSalida', 
                                parFuncionario = '$this->parFuncionario'                            
                        WHERE idParqueoCarros = $this->idParqueoCarros  
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "DELETE FROM 
                            parqueocarros 
                        WHERE 
                            idParqueoCarros = $this->idParqueoCarros";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT
                                idParqueoCarros
                                ,parPlaca
                                ,parFechaHoraIngreso
                                ,parFechaHoraSalida
                                ,parFuncionario
                            FROM
                                parqueocarros $condicion					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idParqueoCarros !=''){
            $condicion=$condicion.$whereAnd." parqueocarros.idParqueoCarros  = $this->idParqueoCarros";
            $whereAnd = ' AND ';
            
        }else{
            if($this->parPlaca !=''){
                $condicion=$condicion.$whereAnd." parqueocarros.parPlaca  = '$this->parPlaca'";
                $whereAnd = ' AND ';            
            }
            if($this->parFechaHoraIngreso !=''){
                $condicion=$condicion.$whereAnd." parqueocarros.parFechaHoraIngreso  = '$this->parFechaHoraIngreso'";
                $whereAnd = ' AND ';            
            }
            if($this->parFechaHoraSalida !=''){
                $condicion=$condicion.$whereAnd." parqueocarros.parFechaHoraSalida  = '$this->parFechaHoraSalida'";
                $whereAnd = ' AND ';            
            }
            if($this->parFuncionario !=''){
                $condicion=$condicion.$whereAnd." parqueocarros.parFuncionario  like '%$this->parFuncionario%'";
                $whereAnd = ' AND ';            
            }
            
        }       
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idFuncionario);
        unset($this->funIdentificacion);
        unset($this->funNombres);
        unset($this->funApellidos);
        unset($this->funCorreo);
        unset($this->funCargos);
        unset($this->conn);
    }
}
?>
