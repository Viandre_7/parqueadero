<?php
require_once '../entorno/conexion.php';
require '../modelo/parqueoCarros.M.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $parqueoCarros= new ParqueoCarros();
                $parqueoCarros->setParPlaca($_POST['placa']);
                $parqueoCarros->setParFechaHoraIngreso($_POST['ingreso']);
                $parqueoCarros->setParFechaHoraSalida($_POST['salida']);
                $parqueoCarros->setParFuncionario($_POST['funcionario']);
                $resultado = $parqueoCarros->adicionar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $parqueoCarros= new ParqueoCarros();
                $parqueoCarros->setIdParqueoCarros($_POST['idParqueoCarros']);
                $parqueoCarros->setParPlaca($_POST['placa']);
                $parqueoCarros->setParFechaHoraIngreso($_POST['ingreso']);
                $parqueoCarros->setParFechaHoraSalida($_POST['salida']);
                $parqueoCarros->setParFuncionario($_POST['funcionario']);
                $resultado = $parqueoCarros->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $parqueoCarros= new ParqueoCarros();
                $parqueoCarros->setIdParqueoCarros($_POST['idParqueoCarros']);
                $resultado = $parqueoCarros->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.";                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $parqueoCarros= new ParqueoCarros();
                $parqueoCarros->setIdParqueoCarros($_POST['idParqueoCarros']);
                $parqueoCarros->setParPlaca($_POST['placa']);
                $parqueoCarros->setParFechaHoraIngreso($_POST['ingreso']);
                $parqueoCarros->setParFechaHoraSalida($_POST['salida']);
                $parqueoCarros->setParFuncionario($_POST['funcionario']);
                $resultado = $parqueoCarros->consultar();
                $numeroRegistros = $parqueoCarros->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $parqueoCarros->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idParqueoCarros;
                        $respuesta['placa'] = $rowBuscar->parPlaca;
                        $respuesta['ingreso'] = $rowBuscar->parFechaHoraIngreso;
                        $respuesta['salida'] = $rowBuscar->parFechaHoraSalida;
                        $respuesta['funcionario'] = $rowBuscar->parFuncionario;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="<table> <tr>
                                        <td>Placa</td>
                                        <td>Ingreso</td>
                                        <td>Salida</td>
                                        <td>Funcionario</td>                                
                                        <td>Acción</td>
                                    </tr>";
                        foreach($parqueoCarros->conn->obtenerRegistros() AS $rowConsulta){
                            
                            $retorno .= "                                           
                                        <tr>                                            
                                        <td><label >".$rowConsulta[1]."</label></td>   
                                        <td><label >".$rowConsulta[2]."</label></td>   
                                        <td><label >".$rowConsulta[3]."</label></td>   
                                        <td><label >".$rowConsulta[4]."</label></td>                                        
                                        <td>
                                            <input type='button' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'>
                                            <input type='button' name='eliminar' class='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'>
                                        </td>
                                        </tr>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $retorno .= "<table>";
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
            }
            //Retornar del retorno
            $respuesta['accion']='CONSULTAR';
            echo json_encode($respuesta);
            break;
    }
}
?>
