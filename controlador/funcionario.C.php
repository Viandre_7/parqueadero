<?php
require_once '../entorno/conexion.php';
require '../modelo/funcionario.M.php';
$respuesta = array();
if (isset ($_POST['accion'])){
    switch($_POST['accion']){
        case 'ADICIONAR':
            try{
                $funcionario= new Funcionario();
                $funcionario->setFunIdentificacion($_POST['identificacion']);
                $funcionario->setFunNombres($_POST['nombres']);
                $funcionario->setFunApellidos($_POST['apellidos']);
                $funcionario->setFunCorreo($_POST['correo']);
                $funcionario->setFunCargo($_POST['cargo']);
                $resultado = $funcionario->adicionar();
                    
                    $respuesta['respuesta'] = "La información se adicionó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible adicionar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='ADICIONAR'; 
                echo json_encode($respuesta);
            break;
        case 'MODIFICAR':
            try{
                $funcionario= new Funcionario();
                $funcionario->setIdFuncionario($_POST['idFuncionario']);
                $funcionario->setFunIdentificacion($_POST['identificacion']);
                $funcionario->setFunNombres($_POST['nombres']);
                $funcionario->setFunApellidos($_POST['apellidos']);
                $funcionario->setFunCorreo($_POST['correo']);
                $funcionario->setFunCargo($_POST['cargo']);
                $resultado = $funcionario->modificar();
                    $respuesta['respuesta'] = "La información se modificó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible modificar la información, consulte con el administrador.";
            }

                //Respuesta del retorno
                $respuesta['accion']='MODIFICAR'; 
                echo json_encode($respuesta);
            break;
        case 'ELIMINAR':
            try{
                $funcionario= new Funcionario();
                $funcionario->setIdFuncionario($_POST['idFuncionario']);
                $resultado = $funcionario->eliminar();

                $respuesta['respuesta'] = "La información se eliminó correctamente.";
                }catch(Exception $e){
                    $respuesta['respuesta'] ="Error, no fué posible eliminar la información, consulte con el administrador.";                    
                }

                //Respuesta del retorno
                $respuesta['accion']='ELIMINAR'; 
                echo json_encode($respuesta);
            break;
        case 'CONSULTAR':
            try{
                $funcionario= new Funcionario();
                $funcionario->setIdFuncionario($_POST['idFuncionario']);
                $funcionario->setFunIdentificacion($_POST['identificacion']);
                $funcionario->setFunNombres($_POST['nombres']);
                $funcionario->setFunApellidos($_POST['apellidos']);
                $funcionario->setFunCorreo($_POST['correo']);
                $funcionario->setFunCargo($_POST['cargo']);
                $resultado = $funcionario->consultar();
                $numeroRegistros = $funcionario->conn->obtenerNumeroRegistros();
                $respuesta['numeroRegistros']=$numeroRegistros;

                if($numeroRegistros === 1){
                    if ($rowBuscar = $funcionario->conn->obtenerObjeto()){
                        $respuesta['id'] = $rowBuscar->idFuncionario;
                        $respuesta['identificacion'] = $rowBuscar->funIdentificacion;
                        $respuesta['nombres'] = $rowBuscar->funNombres;
                        $respuesta['apellidos'] = $rowBuscar->funApellidos;
                        $respuesta['correo'] = $rowBuscar->funCorreo;
                        $respuesta['cargo'] = $rowBuscar->funCargo;
                    }
                }else{
                    if(isset($resultado)){
                        $retorno="<table> <tr>
                                        <td>Identificación</td>
                                        <td>nombres</td>
                                        <td>apellidos</td>
                                        <td>correo</td>
                                        <td>cargo</td>
                                        <td>Acción</td>
                                    </tr>";
                        foreach($funcionario->conn->obtenerRegistros() AS $rowConsulta){
                            
                            $retorno .= "                                           
                                        <tr>                                            
                                        <td><label >".$rowConsulta[1]."</label></td>   
                                        <td><label >".$rowConsulta[2]."</label></td>   
                                        <td><label >".$rowConsulta[3]."</label></td>   
                                        <td><label >".$rowConsulta[4]."</label></td>
                                        <td><label >".$rowConsulta[5]."</label></td>
                                        <td>
                                            <input type='button' name='editar' value='Editar' onclick='Enviar(\"CONSULTAR\",".$rowConsulta[0].")'>
                                            <input type='button' name='eliminar' class='eliminar' value='Eliminar' onclick='Enviar(\"ELIMINAR\",".$rowConsulta[0].")'>
                                        </td>
                                        </tr>";                            
                        }                                        
                        $respuesta['tablaRegistro']=$retorno;
                        
                    }else{
                        $retorno .= "<table>";
                        $respuesta['tablaRegistro']='No existen datos!!!';
                    }
                }

            }catch(Exception $e){
                echo "hola";
            }
            //Retornar del retorno
            $respuesta['accion']='CONSULTAR';
            echo json_encode($respuesta);
            break;
    }
}
?>
