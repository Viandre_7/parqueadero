function Enviar(accion,id){
    var parametros = {        
        "accion" : accion,
        "dato": $('#txtDato').val(),
        "fila": $('#txtFila').val(),
        "columna": $('#txtColumna').val()
    };

     $.ajax({
            data:  parametros, //datos que se van a enviar al ajax
            url:   '../controlador/Ejemplo.php', //archivo php que recibe los datos
            type:  'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
           
            success:  function (data) { //procesa y devuelve la respuesta                
                if(data['accion']=='ADICIONAR'){
                    alert(data['matriz']);
                    $('#txtFila').val(data['fila']);
                    $('#txtColumna').val(data['columna']);
                }

            }
    });
    
}

function Limpiar(){
    $('#hidAccion').val("");
    $('#txtDato').val("");
}
