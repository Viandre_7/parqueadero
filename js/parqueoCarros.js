function Enviar(accion,id){
    if(id==null){
        id=$('#hidParqueoCarros').val();
    }
    var parametros = {
        "idParqueoCarros" :id,
        "accion" : accion,
        "placa": $('#txtPlaca').val(),
        "ingreso": $('#txtIngreso').val(),
        "salida": $('#txtSalida').val(),
        "funcionario": $('#textFuncionario').val()
    };

     $.ajax({
            data:  parametros, //datos que se van a enviar al ajax
            url:   '../controlador/parqueoCarros.C.php', //archivo php que recibe los datos
            type:  'post', //método para enviar los datos
            dataType: 'json',//Recibe el array desde php
           
            success:  function (data) { //procesa y devuelve la respuesta
                
                if(data['accion']=='ADICIONAR'){
                    alert(data['respuesta']);
                }
                
                if(data['accion']=='CONSULTAR' && data['numeroRegistros']>1){
                        $("#resultado").html(data['tablaRegistro']);
                }else{
                    $('#hidParqueoCarros').val(data['id']);
                    $('#txtPlaca').val(data['placa']);
                    $('#txtIngreso').val(data['ingreso']);
                    $('#txtSalida').val(data['salida']);
                    $('#textFuncionario').val(data['funcionario']);
                }
                if(data['accion']=='MODIFICAR'){
                    alert(data['respuesta']);
                }
                if(data['accion']=='ELIMINAR'){
                    alert(data['respuesta']);
                }
            }
    });
    
}

function Limpiar(){
    $('#hidIdParqueoCarros').val("");
    $('#txtPlaca').val("");
    $('#txtIngreso').val("");
    $('#txtSalida').val("");
    $('#textFuncionario').val("");
}
