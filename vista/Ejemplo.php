<!DOCTYPE>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> Acción</title>
    <script type="text/javascript" language="javascript" src="../js/Ejemplo.js" ></script>
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
</head>

<body>
    <form name="accion" id="accion">        
        <div>
            <p>Identificación: </p>
            <input type="number" name="txtDato" id="txtDato" value="">
            <input type="text" name="txtFila" id="txtFila" value="">
            <input type="text" name="txtColumna" id="txtColumna" value="">
        </div>

        <input type="button" name="btnAccion" id="btnAdicionar" value="Adicionar" onclick="Enviar('ADICIONAR',null)"/>
        <input type="button" name="btnAccion" id="btnModificar" value="Modificar" onclick="Enviar('MODIFICAR',null)"/>
        <input type="button" name="btnAccion" id="btnEliminar" value="Eliminar"   onclick="Enviar('ELIMINAR',null)"/>
        <input type="button" name="btnAccion" id="btnConsultar" value="Consultar" onclick="Enviar('CONSULTAR',null)"/>
        <input type="button" name="btnAccion" id="btnLimpiar" value="Limpiar" onclick="Limpiar()"/>

        <div id="resultado"></div>
    </form>
</body>
</html>